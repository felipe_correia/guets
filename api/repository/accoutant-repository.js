'use strict';
const mongoose = require('mongoose'),
    Accountant = require('../models/accountant-registration').Accountant;


exports.create = async (data) => {
    let accountant = new Accountant(data);
    return await accountant.save();
}

exports.update = async (id, data) => {
    
    let accountant = new Accountant(data);

    await Accountant.findByIdAndUpdate(id, {
        
        $set: {
            date: accountant.date,
            count: accountant.count,
            id_device: accountant.id_device,
        }
    });
}


exports.getById = async (id) => {
    const res = await Accountant
        .findById(id);
    return res;
}