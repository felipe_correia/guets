'use strict';
const mongoose = require('mongoose'),
    Trucker = require('../models/trucker').Trucker;

exports.get = async () => {
    const res = await Trucker.find({
    });
    return res;
}

exports.getById = async (id) => {
    const res = await Trucker
        .findById(id);
    return res;
}

exports.create = async (data) => {
    let trucker = new Trucker(data);
    return await trucker.save()
}

exports.update = async (id, data) => {
    let trucker = new Trucker(data);

    await Trucker.findByIdAndUpdate(id, {

        $set: {
            name: req.body.name,
            email: req.body.email,
            CNPJ: req.body.CNPJ,
            CPF: req.body.CPF,
            birth: req.body.birth,
            cellphone: req.body.cellphone,
            profile: req.body.profile,
            subprofile: req.body.subprofile,
            // civilStatus: req.body.civilStatus,
            // gender: req.body.gender,
            typeOfCompany: req.body.typeOfCompany,
            cep: req.body.cep,
        }
    })
}

exports.delete = async (id) => {
    await Trucker
        .findOneAndRemove(id);
}

exports.exists = async (query) => {
    await Trucker.count(query);
}