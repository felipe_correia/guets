const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUiExpress = require('swagger-ui-express');
const glob = require('glob');

const controllers = glob.sync('./controllers/*.js');
const models = glob.sync('./models/*.js');

console.log(controllers, models)

const swaggerConfig = {
 swaggerDefinition: {
   swagger: '2.0',
   info: {
     description: 'Api para sistema Guets.',
     version: '1.0.0',
     title: 'Api Guets',
     contact: {
       email: 'eduardo.fernandes@fcamara.com.br',
     },
   },
   host: 'http://54.89.83.189:3004',
   basePath: '/',
   schemes: ['http'],
 },
 apis: [...models, ...controllers],
};

module.exports = (app) => {
 const docs = swaggerJSDoc(swaggerConfig);
 const options = {
   customSiteTitle: 'Api Guets - Documentação',
 };

 app.use('/docs', swaggerUiExpress.serve, swaggerUiExpress.setup(docs, options));
 app.use('/swagger.json', (req, res) => {
   res.send(docs);
 });
};