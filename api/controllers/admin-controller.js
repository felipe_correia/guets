'use strict';


const Search = require('../models/trucker').Trucker;


exports.post = async (req, res) => {
    // console.log(req.body);
    let obj = {
        result: null,
        count: null
    }
    var queryInteraction = undefined;


    let query = {
    };
    var bodyData = req.body;

    console.log(bodyData.initialDate, bodyData.finalDate);


    if (bodyData.eventName !== undefined && bodyData.eventName !== "")
        query.eventName = bodyData.eventName;

    if (bodyData.initialDate && bodyData.finalDate) {

        query.creationDate = {
            $gt: new Date(bodyData.initialDate),
            $lt: new Date(bodyData.finalDate + "T23:59:59.000Z")
        }
    };

    if (bodyData.initialDate && bodyData.finalDate == undefined) {

        query.creationDate = {
            $gt: new Date(bodyData.initialDate)
        }
    };

    if (bodyData.finalDate && bodyData.initialDate == undefined) {

        query.creationDate = {
            $lt: new Date(bodyData.finalDate + "T23:59:59.000Z")
        }
    };


    console.log(query);

    Search.find(query, (err, list) => {
        if (err) {
            return err;
        }
        if (!list) {
            return res.status(400).send({
                message: 'Failed to load candidate ' + query
            })
        }
        let r = Object(list);
        return res.status(200).send(r);
        // console.log(list);

    })

    // try {

    //     let newAccountant = {
    //         date: req.body.date,
    //         count: req.body.count,
    //         id_device: req.body.id_device,

    //     };

    //     var accountant = await Repository.create(newAccountant);
    //     res.status(200).json(accountant);

    // } catch (err) {
    //     console.log(err)
    //     res.status(500).send(err);
    // }
}

// exports.update = async (req, res) => {
//     try {

//         let newAccountant = {
//             date: req.body.date,
//             count: req.body.count,
//             id_device: req.body.id_device,
//         };

//         var accountantUp = await Repository.update(req.params.id, newAccountant);

//         var accountant = await Repository.getById(req.params.id);

//         res.status(200).json(accountant);
//     } catch (err) {
//         res.status(500).send(err);
//     }
// }

// exports.getById = async (req, res) => {
//     try {
//         var accountant = await Repository.getById(req.params.id);
//         if (accountant) {
//             res.status(200).json(accountant);
//         }
//         else {
//             res.status(200).send("Nenhum Dispositivo foi localizado");
//         }
//     } catch (err) {
//         res.status(500).send(err);
//     }
// }
