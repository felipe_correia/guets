'use strict';


const ValidationContract = require('../validation/validation'),
    Repository = require("../repository/accoutant-repository"),
    Accountant = require('../models/accountant-registration').Accountant;

exports.add = async (req, res) => {
    /**
* @swagger
* /accountant:
*   post:
*     description: Adiciona e atualiza o contador quando é efetuado a sincronização.
*     tags: [Accountant]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/
    let objRes = {
        result: null,
        count: 0
    };

    let query = {
    };

    var bodyData = req.body;
    console.log(bodyData);


    if (bodyData.id_device !== undefined && res.id_device !== "")
        query.id_device = bodyData.id_device;



    await Accountant.find(query, (err, list) => {

        if (err) {

            return err;
        }
        if (!list) {

            return res.status(400).send({
                message: 'Failed to load count ' + query
            })
        }

        objRes.result = list;

    });

    // console.log(objRes.result[0].id_device);

    if (objRes.result != "") {

        if (objRes.result[0].id_device == bodyData.id_device) {
            objRes.result[0].count++;
            console.log("dentro do if", objRes.result[0].count);

            let newAccountant = {
                _id: objRes.result[0].id,
                date: objRes.result[0].date,
                count: objRes.result[0].count,
                id_device: objRes.result[0].id_device,

            };

            let id = objRes.result[0].id;

            var accountantUp = await Repository.update(id, newAccountant);

            res.status(200).send("Updated");

        }

    } else {
        try {

            let newAccountant = {
                date: req.body.date,
                count: req.body.count,
                id_device: req.body.id_device,

            };

            var accountant = await Repository.create(newAccountant);
            console.log(newAccountant);
            res.status(200).json(accountant);

        } catch (err) {
            console.log(err)
            res.status(500).send(err);
        }
    }

}


exports.post = async (req, res) => {
        /**
* @swagger
* /accountant/add:
*   post:
*     description: Adiciona e atualiza o contador.
*     tags: [Accountant]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/

    let objRes = {
        result: null,
        count: 0
    };

    let query = {
    };

    var bodyData = req.body;
    console.log(bodyData);



    for (let res of bodyData) {
        console.log(res);


        if (res.id_device !== undefined && res.id_device !== "")
            query.id_device = res.id_device;


        let re = await Accountant.find(query, (err, list) => {

            if (err) {

                return err;
            }
            if (!list) {

                return res.status(400).send({
                    message: 'Failed to load count ' + query
                })
            }

            return list;

        });

        if (re.length > 0) {
            objRes.result = re;
            console.log("primeiro", objRes.result);


            if (objRes.result[0].id_device == res.id_device) {
                objRes.result[0].count++;
                console.log("dentro do if", objRes.result[0].count);

                let newAccountant = {
                    _id: objRes.result[0].id,
                    date: objRes.result[0].date,
                    count: objRes.result[0].count,
                    id_device: objRes.result[0].id_device,

                };

                let id = objRes.result[0].id;

                var accountantUp = await Repository.update(id, newAccountant);
            }

        } else {
            // console.log("else",res);    
            try {

                let newAccountant = {
                    date: res.date,
                    count: res.count,
                    id_device: res.id_device,

                };
                console.log("newAccountant", newAccountant);

                var accountant = await Repository.create(newAccountant);

                // return res.status(200).send(accountant);

            } catch (err) {
                console.log(err)
                return res.status(500).send(err);
            }
        }
    }
    return res.send("atualizado");
}


exports.update = async (req, res) => {
        /**
* @swagger
* /accountant/:id:
*   put:
*     description: Atualiza o contador
*     tags: [Accountant]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/
    try {

        let newAccountant = {
            date: req.body.date,
            count: req.body.count,
            id_device: req.body.id_device,
        };

        var accountantUp = await Repository.update(req.params.id, newAccountant);

        var accountant = await Repository.getById(req.params.id);

        res.status(200).json(accountant);
    } catch (err) {
        res.status(500).send(err);
    }
}

exports.getById = async (req, res) => {
        /**
* @swagger
* /accountant/:id:
*   get:
*     description: Encontra um contador
*     tags: [Accountant]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/
    let objRes = {
        result: null,
        count: 0
    };

    let query = {
    };
    console.log(req.params.id);

    let id_device = req.params.id;


    if (id_device !== undefined && id_device)
        query.id_device = id_device;

    let arr = await Accountant.find(query, (err, list) => {

        if (err) {
            console.log("erro", err)
            return err;
        }
        if (list == "") {
            console.log(list)

            return res.status(400).send({
                message: 'Failed to load count ' + query
            })
        }

        return list;

    });
    // objRes.result = arr;

    if (arr) {
        objRes.result = arr;
    } else {
        return res.status(400).send({
            message: 'Failed to load count ' + query
        })
    }


    if (objRes.result) {
        if (objRes.result.length > 0) {
            if (objRes.result[0]._id !== "") {
                let cod = objRes.result[0]._id;

                var accountant = await Repository.getById(cod);
                if (accountant) {
                    return res.status(200).json(accountant);
                } else {
                    return res.status(500).send("No Device Found");
                }

            } else {
                return res.status(500).send({
                    message: 'No Device Found'
                });
            }
        }

    }

}
