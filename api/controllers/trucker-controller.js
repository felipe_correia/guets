'use strict';

var XLSX = require('xlsx');
// var data = "a,b,c\n1,2,3".split("\n").map(function(x) { return x.split(","); });



const ValidationContract = require('../validation/validation'),
    Repository = require("../repository/trucker-repository"),
    Model = require('../models/trucker'),
    buscaCep = require('busca-cep');

exports.get = async (req, res) => {
    /**
* @swagger
* /trucker:
*   get:
*     description: Encontra todos cadastros.
*     tags: [Trucker]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/

    try {
        var data = await Repository.get();
        // for (let res of data2) {
        //     var data = res.name + "\n" + res.email+ "\n"+ res.CPF + "\n" + res.birth + "\n" + res.cellphone.split("\n").map(function (x) { return x; });
        //     var ws = XLSX.utils.aoa_to_sheet(data);
        //     var wb = XLSX.utils.book_new();
        //     XLSX.utils.book_append_sheet(wb, ws, "Lucas");
        // }



        // console.log(ws);

        return res.status(200).json(data);
    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar aquisição'
        });
    }
}

exports.getById = async (req, res, next) => {
    /**
* @swagger
* /trucker/:id:
*   get:
*     description: Encontra um cadastro pelo ID.
*     tags: [Trucker]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/
    try {
        var data = await Repository.getById(req.params.id);
        res.status(200).send(data);
    } catch (err) {
        res.status(500).send({
            message: 'Falha ao processar aquisição'
        });
    }
}

exports.post = async (req, res) => {
    /**
* @swagger
* /trucker:
*   post:
*     description: Salva um novo cadastro.
*     tags: [Trucker]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/

    let contract = new ValidationContract();
    let noSpecialsCell = await contract.removeSpecials(req.body.cellphone);
  
    contract.hasMinLen(req.body.name, 3, 'O nome deve conter pelo menos 3 caracteres');
    contract.isEmail(req.body.email, 'E-mail inválido')
    contract.hasMinLen(req.body.cellphone, 9, 'O número de celular deve conter 9 caractetes');
    let cpfv = undefined;
    req.body.CPF ? cpfv = contract.isCPF(req.body.CPF) : cpfv = undefined;
    let cnpjv = undefined;
    req.body.CNPJ ? cnpjv = contract.isCNPJ(req.body.CNPJ) : cnpjv = undefined;


    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    

    if (!cpfv && !cnpjv) {
        res.status(400).send(contract.errors()).end();
        return;
    }


    try {

        let newTrucker = {
            name: req.body.name,
            email: req.body.email,
            CNPJ: req.body.CNPJ,
            CPF: req.body.CPF,
            birth: req.body.birth,
            cellphone: noSpecialsCell,
            profile: req.body.profile,
            subprofile: req.body.subprofile,
            // civilStatus: req.body.civilStatus,
            // gender: req.body.gender,
            // numero: req.body.numero,
            // logradouro: cep.logradouro,
            // complemento: req.body.complemento,
            // bairro: cep.bairro,
            localidade: req.body.localidade,
            uf: req.body.uf,
            typeOfCompany: req.body.typeOfCompany,
            creationDate: req.body.creationDate,
            corporateName: req.body.corporateName,
            telephone: req.body.telephone,
            eventName: req.body.eventName,
            truckerModel: req.body.truckerModel,
            truckerBrand: req.body.truckerBrand,
        };

        // console.log(newTrucker);

        var Trucker = await Repository.create(newTrucker);

        res.status(200).json(Trucker);

    } catch (err) {
        console.log(err)
        res.status(500).send(err);
    }
};


// exports.searchCep = async (req, res) => {
    /**
* @swagger
* /trucker/searchcep:
*   post:
*     description: Encontra o CEP pela api dos correios.
*     tags: [Trucker]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/
//     console.log(req.body.cep);

//     let ncep = req.body.cep;
//     let cep = buscaCep(ncep, true);//Também pode ser usado buscaCep('01001-000', {sync: true});

//     try {
//         res.status(200).json(cep);
//     } catch (err) {
//         res.status(500).send({
//             message: 'Falha ao processar aquisição'
//         });
//     }
// }

exports.put = async (req, res, next) => {
    /**
* @swagger
* /trucker/:id:
*   put:
*     description: Atualiza um cadastro pelo ID.
*     tags: [Trucker]
*     produces:
*       - application/json
*     responses:
*       200:
*         description:
*/
    try {
        await Repository.update(req.params.id, req.body);
        res.status(200).send({
            message: 'Caminhoneiro alterado com Sucesso!'
        });

    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar aquisição'
        });

    };
}

exports.delete = async (req, res, next) => {
    /**
* @swagger
* /trucker/:id:
*   delete:
*     description: Deleta um cadastro pelo ID.
*     tags: [Trucker]
*     produces:
*       - application/json
*     responses:
*       200:

*/
    try {
        await Repository.delete(req.body.id);
        res.status(200).send({
            message: 'Caminhoneiro removido com Sucesso!'
        });

    } catch (e) {
        res.status(500).send({
            message: 'Falha ao processar aquisição'
        });
    };
}