'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    /**
* @swagger
* definition:
*   Accountant:
*     type: object
*     properties:
*       date:
*         type: Date
*         required: true
*       count:
*         type: Number
*         required: true
*       id_device:
*         type: String
*         required: true
*/
    date: {
        type: Date,
        required: true,
    },
    count: {
        type: Number,
        required: true,
    },
    id_device: {
        type: String,
        required: true,
    },

});

module.exports.Accountant = mongoose.model('Accountant', schema);