'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    /**
* @swagger
* definition:
*   Trucker:
*     type: object
*     properties:
*       name:
*         type: String
*         required: true
*         trim: true
*       email:
*         type: String
*         required: true
*         trim: true
*       CNPJ:
*         type: String
*         trim: true
*       CPF: 
*         type: String
*         trim: true
*       birth: 
*         type: Date
*         required: true
*       cellphone: 
*         type: String
*         trim: true
*       profile:
*         type: String
*         required: true
*       subprofile: 
*         type: String
*       bairro: 
*         type: String
*       localidade: 
*         type: String
*       uf: 
*         type: String
*       typeOfCompany: 
*         type: String
*       creationDate: 
*         type: Date
*         default: undefined
*       corporateName: 
*         type: String
*         trim: true
*       telephone: 
*         type: String
*         trim: true
*       eventName: 
*         type: String
*         trim: true
*/
    name: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
    },
    CNPJ: {
        type: String,
        trim: true,
    },
    CPF: {
        type: String,
        trim: true,
    },
    birth: {
        type: Date,
        required: true,
    },
    cellphone: {
        type: String,
        // required: true, 
        trim: true,
    },
    profile: {
        type: String,
        required: true,
    },
    subprofile: {
        type: String
    },
    // cep: {
    //     type: String,
    //     required: true,
    //     trim: true,
    // },
    // numero:{
    //     type: Number,
    //     required: true,
    //     trim: true,
    // },
    // logradouro: {
    //     type: String,
    // },
    // complemento: {
    //     type: String,
    // },
    bairro: {
        type: String,
    },
    localidade: {
        type: String,
    },
    uf: {
        type: String,
    },
    typeOfCompany: {
        type: String,
    },
    // gender: {
    //     type: String,
    // },
    // civilStatus: {
    //     type: String,
    // },
    creationDate: {
        type: Date,
        default: undefined,
    },
    corporateName: {
        type: String,
        trim: true,
    },
    telephone: {
        type: String,
        trim: true,
    },
    eventName: {
        type: String,
        trim: true,
    },
    truckerModel: {
        type: String,
        trim: true,
    },
    truckerBrand: {
        type: String,
        trim: true,
    }


});

module.exports.Trucker = mongoose.model('Trucker', schema);