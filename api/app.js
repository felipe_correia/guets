'use restrict';

const express = require('express')
, cors            = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
const router = express.Router();
const config = require('./config');
var XLSX = require('xlsx');



app.use(cors({ origin: '*' }));

require('./middlewares/swagger')(app)
//Conecta Banco
mongoose.connect(config.connectionString);

//Carrega Models
const Trucker = require('./models/trucker');
const Accountant = require('./models/accountant-registration');



// Carrega rotas
const indexRoute = require('./routes/index-route');
const truckerRoute = require('./routes/trucker-route');
const accountantRoute = require('./routes/accountant-route');
const adminRoute = require('./routes/admin-route');
const email = require('./routes/email-route');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));



app.use('/', indexRoute);
app.use('/trucker', truckerRoute);
app.use('/accountant', accountantRoute);
app.use('/admin', adminRoute);
app.use('/email', email);



module.exports = app;