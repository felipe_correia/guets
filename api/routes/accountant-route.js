'use restrict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/accountant-controller');

router.post('/', controller.post);
router.get('/:id',controller.getById);
router.put('/:id',controller.update);
router.post('/add', controller.add);


module.exports = router;