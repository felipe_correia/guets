'use restrict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/trucker-controller');

router.get('/', controller.get);
router.get('/:id', controller.getById);
router.post('/', controller.post);
// router.post('/searchcep', controller.searchCep);
router.put('/:id', controller.put);
router.delete('/:id', controller.delete);



module.exports = router;