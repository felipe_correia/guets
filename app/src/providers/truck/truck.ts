import { CountModel } from '../../Models/count';
import { TruckerList } from '../../Models/trucker-list';
import { TruckerModel } from '../../Models/trucker';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class TruckProvider {

  private baseApiPath = "http://54.89.83.189:3004";
  // private baseApiPath = "http://localhost:3003";
  private statusIternet: Subject<boolean> = new Subject<boolean>();
  data: TruckerList = new TruckerList();

  constructor(
    public http: Http,
    private storage: Storage,
    private datepipe: DatePipe) {
  }

  setStatus(value: boolean) {
    // console.log(value);
    this.statusIternet.next(value);
  }

  getStatus(): Observable<boolean> {
    // console.log("teste");
    return this.statusIternet.asObservable();
  }

  getTrucker() {
    return this.http.get(this.baseApiPath + "/trucker")
    .map((res) => res.json())
    .catch(e => {
      console.log(e.status);
      return Observable.throw({"Errors": e.json(status)});
    });
  }
  
  postTruckerStorage(truck: TruckerModel){
    return this.http.post(this.baseApiPath + "/trucker", truck);
  }

  postTrucker(truck: TruckerModel) {
    // console.log(this.baseApiPath);
    let w = 200;
    return this.http.post(this.baseApiPath + "/trucker", truck)
                    // .map(response => response.json(), w)
                    .toPromise()
                    .catch((error: any) => {
                      // console.log(error);
                      return Observable.throw(error);
                    });
  }

  // async searchCep(returnCep: TruckerModel): Promise<any> {
  //   return await this.http.post(this.baseApiPath + "/trucker/searchcep", returnCep)
  //     // .map(response => response.json())
  //     .toPromise()
  //     .catch((error: any) => {
  //       return Observable.throw(error);
  //     })
      
  // }


  public insertStorage(trucker: TruckerModel, count:CountModel) {
    let data: TruckerList[] = [];
   
    this.data.trucker.push(trucker);
    this.data.count.push(count);

    data.push(this.data);

    console.log(this.data);

    let key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
    return this.saveStorage(key, this.data);
  }

  public getAll() {
    let truckers: TruckerList[] = [];

    return this.storage.forEach((value: TruckerList, key: string, itratiobnNumber: Number) => {
      let date = new TruckerList();
      date.key = key;
      date.count = value.count;
      date.trucker = value.trucker;
      truckers.push(date);
    })
      .then(() => {
        return Promise.resolve(truckers);
      })
      .catch((error) => {
        return Promise.reject(error);

      });

  }

  // public insertStorage2(tot:number) {
  //   let key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
  //   return this.saveStorage2(key, tot);
  // }

  public updatedStorage(key: string, object: TruckerList) {
    return this.saveStorage(key, object);

  }

  public saveStorage(key: string, object: TruckerList) {
    return this.storage.set(key, object);
  }

  // public saveStorage2(key: string, tot:number) {
  //   return this.storage.set(key, tot);
  // }


  public removeStorage(key: TruckerList) {
    let t: any = Object(key);
    for (let i = 0; i < t.length; i++) {
      this.storage.remove(key[i].key);
    }
    return;
  }



}
