import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { searchTruckerModel } from '../../Models/search-trucker';

/*
  Generated class for the AdminProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AdminProvider {

  private baseApiPath = "http://54.89.83.189:3004";
  // private baseApiPath = "http://localhost:3003";

  constructor(public http: Http) {
    // console.log('Hello AdminProvider Provider');
  }

  postTruckerStorage(truck: searchTruckerModel){
    return this.http.post(this.baseApiPath + "/admin", truck);
  }

  searchTruckerStorageByDate(request) {
    let query: string = `${this.baseApiPath}/admin`;
    return this.http.post(query, JSON.stringify(request))
      .map((res) => res.json());
  }
  
}
