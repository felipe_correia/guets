import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { EmailModel } from '../../Models/email';

/*
  Generated class for the EmailProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EmailProvider {

  // private baseApiPath = "http://localhost:3003/email";
  private baseApiPath = "http://54.89.83.189:3004/email";


  constructor(public http: Http) { }

  send(emailModel: EmailModel){


    return this.http.post(this.baseApiPath, emailModel);
  }

}
