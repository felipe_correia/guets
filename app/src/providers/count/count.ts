import { Observable } from 'rxjs/Observable';
import { CountModel } from '../../Models/count';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CountProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CountProvider {

    private baseApiPath = "http://54.89.83.189:3004/accountant";
    // private baseApiPath = "http://localhost:3003/accountant";

  constructor(public http: Http) {

  }


  getCount(){
    return this.http.get(this.baseApiPath);
  }

  getById(id: string){
    let urlGetById = `${this.baseApiPath}/${id}`;
    return this.http.get(urlGetById).map((res) => res.json());
  }

  countSynchronize(count: any){
    console.log(count);
    return this,this.http.post(this.baseApiPath, count)
            .toPromise()
            .catch((error: any)=>{
              return Observable.throw(error);
            });
  }

  countAdd(count: any){
    console.log(count);
    return this,this.http.post(`${this.baseApiPath}/add`, count)
            .toPromise()
            .catch((error: any)=>{
              return Observable.throw(error);
            });
  }

  update(count: CountModel) {
    let urlPut = `${this.baseApiPath}/${count._id}`;
    return this.http.put(urlPut, JSON.stringify(count))
      .map((res) => res.json())
      .catch(e => {
        console.log(e.status);
        return Observable.throw({ "Errors": e.json(status) });
      });
  }


}
