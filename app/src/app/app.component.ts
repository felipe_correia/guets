import { AppState } from './app.global';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Subject } from 'rxjs';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { LoginPage } from '../pages/login/login';
import { FeedPage } from '../pages/feed/feed';
import { FrequentlyQuestionPage } from '../pages/frequently-question/frequently-question';
import { SuggestionPage } from '../pages/suggestion/suggestion';
import { TermsPage } from '../pages/terms/terms';
import { MyGuetsPage } from '../pages/my-guets/my-guets';
import { ReportAbusePage } from '../pages/report-abuse/report-abuse';
import { InvitationPage } from '../pages/invitation/invitation';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage:any = LoginPage;

  // constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private screenOrientation: ScreenOrientation) {
  //   platform.ready().then(() => {
  //     // Okay, so the platform is ready and our plugins are available.
  //     // Here you can do any higher level native things you might need.
  //     statusBar.styleDefault();
  //     splashScreen.hide();
  //   });

  //   // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

  // }

  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  activePage = new Subject();

  pages: Array<{ title: string, component: any, active: boolean, icon: string }>;
  rightMenuItems: Array<{ icon: string, active: boolean }>;
  state: any;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashscreen: SplashScreen,
    public global: AppState,
    public menuCtrl: MenuController
  ) {
    this.initializeApp();
    this.rightMenuItems = [
      { icon: 'home', active: true },
      { icon: 'share', active: false },
      { icon: 'settings', active: false },
      { icon: 'construct', active: false },
      { icon: 'list', active: false },
      { icon: 'help', active: false },
      { icon: 'alert', active: false },
      { icon: 'power', active: false },
    ];

    this.pages = [
      { title: 'Home', component: FeedPage, active: true, icon: 'home' },
      { title: 'Publicidade', component: FeedPage, active: false, icon: 'share' },
      { title: 'Configurações', component: FeedPage, active: false, icon: 'settings' },
      { title: 'Termos de uso e privacidade', component: TermsPage, active: false, icon: 'construct' },
      { title: 'Sugestões', component: SuggestionPage, active: false, icon: 'list' },
      { title: 'Ajuda e Suporte', component: FrequentlyQuestionPage, active: false, icon: 'help' },
      { title: 'Denuncie abuso', component: ReportAbusePage, active: false, icon: 'alert' },
      { title: 'Sair', component: LoginPage, active: false, icon: 'power' },

    ];

    this.activePage.subscribe((selectedPage: any) => {
      this.pages.map(page => {
        page.active = page.title === selectedPage.title;
      });
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.global.set('theme', '');
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashscreen.hide();
      this.menuCtrl.enable(false, 'right');
    });
  }
  my() {
    this.nav.push(MyGuetsPage);
  }
  invitation() {
    this.nav.push(InvitationPage);
  };
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    this.activePage.next(page);
  }

  rightMenuClick(item) {
    this.rightMenuItems.map(menuItem => menuItem.active = false);
    item.active = true;
  }

}

