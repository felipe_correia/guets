import { ProfilePage } from './../pages/profile/profile';
import { ProfileConfigPage } from '../pages/profile-config/profile-config';
import { FeedPage } from './../pages/feed/feed';
import { MyReviewsPage } from './../pages/my-reviews/my-reviews';
import { ProfileDetailsPage } from '../pages/profile-details/profile-details';
import { ProfileInfoPage } from '../pages/profile-info/profile-info';
import { NotificationPage } from '../pages/notification/notification';
import { NotificationTradePage } from './../pages/notification-trade/notification-trade';
import { HomeMenuPage } from './../pages/home-menu/home-menu';
import { LoginMasterPage } from './../pages/login-master/login-master';
import { FrequentlyQuestionPage } from '../pages/frequently-question/frequently-question';
import { MarketingPage } from '../pages/marketing/marketing';
import { SuggestionPage } from '../pages/suggestion/suggestion';
import { TermsPage } from '../pages/terms/terms';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { BrMaskerModule } from 'brmasker-ionic-3';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PostPage } from '../pages/post/post';
import { TipsPage } from '../pages/tips/tips';
import { RegisterPage } from '../pages/register/register';
import { TruckProvider } from '../providers/truck/truck';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common'
import { CountProvider } from '../providers/count/count';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { AdminProvider } from '../providers/admin/admin';
import { File } from '@ionic-native/file';
import { EmailProvider } from '../providers/email/email';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { AppState } from './app.global';
import { WishlistPage } from '../pages/wishlist/wishlist';
import { PromotionPage } from '../pages/post-promotion/post-promotion';
import { PostLotteryPage } from '../pages/post-lottery/post-lottery';
import { PostExclusivePage } from '../pages/post-exclusive/post-exclusive';
import { PostGiftPage } from '../pages/post-gift/post-gift';
import { MyGuetsPage } from '../pages/my-guets/my-guets';
import { TopGuetsPage } from '../pages/top-guets/top-guets';
import { PostWishPage } from '../pages/post-wish/post-wish';
import { AcceptExclusivePage } from '../pages/accept-exclusive/accept-exclusive';
import { ConfirmExclusivePage } from '../pages/confirm-exclusive/confirm-exclusive';
import { ReportAbusePage } from '../pages/report-abuse/report-abuse';
import { InvitationPage } from '../pages/invitation/invitation';
import { PostGiftSendPage } from '../pages/post-gift-send/post-gift-send';


@NgModule({
  declarations: [
    MyApp,
    RegisterPage,
    HomePage,
    LoginPage,
    PostPage,
    PromotionPage,
    PostLotteryPage,
    TipsPage,
    ProfilePage,
    ProfileConfigPage,
    PostExclusivePage,
    PostGiftPage,
    FeedPage,
    MyReviewsPage,
    ProfileDetailsPage,
    ProfileInfoPage,
    NotificationPage,
    NotificationTradePage,
    HomeMenuPage,
    MyGuetsPage,
    LoginMasterPage,
    FrequentlyQuestionPage,
    MarketingPage,
    SuggestionPage,
    TermsPage,
    WishlistPage,
    ReportAbusePage,
    TopGuetsPage,
    PostWishPage,
    AcceptExclusivePage,
    ConfirmExclusivePage,
    InvitationPage,
    PostGiftSendPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
    }),
    HttpModule,
    SelectSearchableModule,
    BrMaskerModule,
    IonicStorageModule.forRoot(),


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RegisterPage,
    HomePage,
    LoginPage,
    PostPage,
    PromotionPage,
    PostLotteryPage,
    TipsPage,
    ProfilePage,
    ProfileConfigPage,
    PostExclusivePage,
    PostGiftPage,
    FeedPage,
    MyReviewsPage,
    ProfileDetailsPage,
    ProfileInfoPage,
    NotificationPage,
    NotificationTradePage,
    HomeMenuPage,
    MyGuetsPage,
    LoginMasterPage,
    FrequentlyQuestionPage,
    MarketingPage,
    SuggestionPage,
    TermsPage,
    WishlistPage,
    ReportAbusePage,
    TopGuetsPage,
    PostWishPage,
    AcceptExclusivePage,
    ConfirmExclusivePage,
    InvitationPage,
    PostGiftSendPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AppState,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    TruckProvider,
    ScreenOrientation,
    Network,
    DatePipe,
    RegisterPage,
    CountProvider,
    UniqueDeviceID,
    AdminProvider,
    File,
    EmailProvider,
    SelectSearchableModule

  ]
})
export class AppModule { }
