import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { TipsPage } from '../tips/tips';
import { PostPage } from '../post/post';
import { PromotionPage } from '../post-promotion/post-promotion';
import { PostLotteryPage } from '../post-lottery/post-lottery';
import { PostExclusivePage } from '../post-exclusive/post-exclusive';
import { PostGiftSendPage } from '../post-gift-send/post-gift-send';

/**
 * Generated class for the HomeMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-post-gift',
  templateUrl: 'post-gift.html',
})
export class PostGiftPage {


  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
  }


  tips() {
    this.navCtrl.push(TipsPage);
  }

  post() {
    this.navCtrl.push(PostPage);
  }

  promotion() {
    this.navCtrl.push(PromotionPage);
  }

  postGiftSend() {
    this.navCtrl.push(PostGiftSendPage);
  }
  lottery() {
    this.navCtrl.push(PostLotteryPage);
  }
  exclusive() {
    this.navCtrl.push(PostExclusivePage);
  }

}
