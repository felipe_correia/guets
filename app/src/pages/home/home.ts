import { CountModel } from './../../Models/count';
import { CountProvider } from './../../providers/count/count';
import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { TruckProvider } from '../../providers/truck/truck';
import { TruckerList } from '../../Models/trucker-list';
import { TruckerModel } from '../../Models/trucker';
import { ProfilePage } from '../profile/profile';
import { FeedPage } from '../feed/feed';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';
import { EmailProvider } from '../../providers/email/email';
import { EmailModel } from '../../Models/email';
import { UniqueDeviceID } from '../../../node_modules/@ionic-native/unique-device-id';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public emailProvider: EmailProvider,
    public file: File,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public truckProvider: TruckProvider,
    public navParams: NavParams) {
    this.menuCtrl.enable(false, 'menu-material');

  }

  ngOnInit() {

  }

  Login() {
    this.navCtrl.push(FeedPage);
  }
  NewProfile() {
    this.navCtrl.push(ProfilePage);
  }
  OpenFacebook() {
    window.open('https://apps.facebook.com/', '_system', 'location=yes');
  };
}
