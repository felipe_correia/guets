import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { TruckProvider } from '../../providers/truck/truck';
import { File } from '@ionic-native/file';
import { EmailProvider } from '../../providers/email/email';

@Component({
  selector: 'page-report-abuse',
  templateUrl: 'report-abuse.html'
})
export class ReportAbusePage {

  constructor(
    public emailProvider: EmailProvider,
    public file: File,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public truckProvider: TruckProvider,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {

  }

}
