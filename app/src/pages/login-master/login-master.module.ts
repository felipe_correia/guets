import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular/umd';
import { LoginMasterPage } from './login-master';
import { RegisterPage } from '../register/register';

@NgModule({
  declarations: [
    LoginMasterPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginMasterPage),
    RegisterPage
  ],
})
export class LoginMasterPageModule { }
