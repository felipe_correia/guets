import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { ProfileDetailsPage } from '../profile-details/profile-details';

/**
 * Generated class for the login-masterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-master',
  templateUrl: 'login-master.html',
})
export class LoginMasterPage {

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(false, 'menu-material');
  }

  ngOnInit() {

  }

  login() {
    this.navCtrl.push(ProfileDetailsPage);
  }
}
