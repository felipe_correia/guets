import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { TruckerModel } from '../../Models/trucker';
import { FormBuilder, Validators } from '@angular/forms';
declare var google;

/**
 * Generated class for the top-guetsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-top-guets',
  templateUrl: 'top-guets.html',
})
export class TopGuetsPage {

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {

  }
}
