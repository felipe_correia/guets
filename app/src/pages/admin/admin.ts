// import { Tab1, Tab2 } from './admin';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConsultPage } from '../consult/consult';

/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {

  tab1: any;
  tab2: any;
  tab3: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tab1 = ConsultPage;
    this.tab3 = Tab3;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AdminPage');
  }

}


//-------------TAB1--------------

// @Component({
//   template: `
//   <ion-header>
//     <ion-navbar>
//       <ion-title>Consulta de cadastros</ion-title>
//     </ion-navbar>
//   </ion-header>
//   <ion-content>Consulta de cadastros</ion-content>`
// })
// export class Tab1 {

//   constructor(public navCtrl: NavController) {
//     // Id is 1, nav refers to Tab1
//     console.log(this.navCtrl.id)
//   }
//  }


 //-------------TAB2--------------

// @Component({
//   template: `
//   <ion-header>
//     <ion-navbar>
//       <ion-title>Total de cadastro</ion-title>
//     </ion-navbar>
//   </ion-header>
//   <ion-content>Total de cadastros por dia</ion-content>`
// })
// export class Tab2 { 
  
//   constructor(public navCtrl: NavController) {
//     // Id is 2, nav refers to Tab2
//     console.log(this.navCtrl.id)
//   }
// }

 //-------------TAB2--------------

@Component({
  template: `
  <ion-header>
    <ion-navbar>
      <ion-title>Exportar para e-mail</ion-title>
    </ion-navbar>
  </ion-header>
  <ion-content>Exportar</ion-content>`
})
export class Tab3 { 
  
  constructor(public navCtrl: NavController) {
    // Id is 3, nav refers to Tab3
    console.log(this.navCtrl.id)
  }
}