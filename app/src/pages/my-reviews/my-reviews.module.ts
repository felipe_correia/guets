import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular/umd';
import { MyReviewsPage } from './my-reviews';
import { RegisterPage } from '../register/register';

@NgModule({
  declarations: [
    MyReviewsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyReviewsPage),
    RegisterPage
  ],
})
export class MyReviewsPageModule {}
