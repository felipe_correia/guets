import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-my-reviews',
  templateUrl: 'my-reviews.html',
})
export class MyReviewsPage {

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
  }
}
