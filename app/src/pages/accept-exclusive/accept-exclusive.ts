import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { ProfileInfoPage } from '../profile-info/profile-info';
import { MyReviewsPage } from '../my-reviews/my-reviews';
import { NotificationTradePage } from '../notification-trade/notification-trade';
import { WishlistPage } from '../wishlist/wishlist';
import { MarketingPage } from '../marketing/marketing';
import { FeedPage } from '../feed/feed';
import { ConfirmExclusivePage } from '../confirm-exclusive/confirm-exclusive';

/**
 * Generated class for the accept-exclusivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-accept-exclusive',
  templateUrl: 'accept-exclusive.html',
})
export class AcceptExclusivePage {
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
  }

  feed() {
    this.navCtrl.push(FeedPage);
  }
  accepted() {
    this.navCtrl.push(ConfirmExclusivePage);
  }
}
