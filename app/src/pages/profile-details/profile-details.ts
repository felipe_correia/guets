import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { ProfileInfoPage } from '../profile-info/profile-info';
import { TruckerModel } from '../../Models/trucker';
import { FormBuilder, Validators } from '@angular/forms';
import { MyReviewsPage } from '../my-reviews/my-reviews';
import { NotificationTradePage } from '../notification-trade/notification-trade';
import { WishlistPage } from '../wishlist/wishlist';
import { MarketingPage } from '../marketing/marketing';

/**
 * Generated class for the profile-detailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-details',
  templateUrl: 'profile-details.html',
})
export class ProfileDetailsPage {
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
  }

  ProfileInfo() {
    this.navCtrl.push(ProfileInfoPage);
  }
  review() {
    this.navCtrl.push(MyReviewsPage);
  }
  wishlist() {
    this.navCtrl.push(WishlistPage);
  }
  notification() {
    this.navCtrl.push(NotificationTradePage);
  }
  marketing() {
    this.navCtrl.push(MarketingPage);
  }

}
