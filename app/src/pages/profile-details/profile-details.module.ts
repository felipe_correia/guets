import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular/umd';
import { ProfileDetailsPage } from './profile-details';
import { RegisterPage } from '../register/register';

@NgModule({
  declarations: [
    ProfileDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileDetailsPage),
    RegisterPage
  ],
})
export class ProfileDetailsPageModule {}
