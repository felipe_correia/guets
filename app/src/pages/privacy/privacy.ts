import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TruckProvider } from '../../providers/truck/truck';

@Component({
    selector: 'page-privacy',
    templateUrl: 'privacy.html'
})
export class PrivacyPage {
    status: boolean;

    constructor(public navCtrl: NavController,
        private truckprovider: TruckProvider) {

    }

    ngOnInit() {

    }


    closePrivacyTerms() {
        this.navCtrl.pop();
    }

}
