import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular/umd';
import { ProfilePage } from './profile';
import { RegisterPage } from '../register/register';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    RegisterPage
  ],
})
export class ProfilePageModule {}
