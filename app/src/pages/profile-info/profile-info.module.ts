import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular/umd';
import { ProfileInfoPage } from './profile-info';
import { RegisterPage } from '../register/register';

@NgModule({
  declarations: [
    ProfileInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileInfoPage),
    RegisterPage
  ],
})
export class ProfileInfoPageModule {}

