import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { TruckerModel } from '../../Models/trucker';
import { FormBuilder, Validators } from '@angular/forms';
declare var google;

/**
 * Generated class for the profile-infoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-info',
  templateUrl: 'profile-info.html',
})
export class ProfileInfoPage {

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {

  }
}
