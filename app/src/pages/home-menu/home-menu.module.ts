import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular/umd';
import { HomeMenuPage } from './home-menu';
import { RegisterPage } from '../register/register';

@NgModule({
  declarations: [
    HomeMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeMenuPage),
    RegisterPage
  ],
})
export class HomeMenuPageModule {}
