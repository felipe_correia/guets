
import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController } from 'ionic-angular';
import { TruckProvider } from '../../providers/truck/truck';
import { File } from '@ionic-native/file';
import { EmailProvider } from '../../providers/email/email';
import { HomeMenuPage } from '../home-menu/home-menu';

@Component({
  selector: 'page-profile-config',
  templateUrl: 'profile-config.html'
})
export class ProfileConfigPage {
  constructor(
    public emailProvider: EmailProvider,
    public file: File,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public truckProvider: TruckProvider,
    public navParams: NavParams) {

    this.menuCtrl.enable(true, 'menu-material');

  }

  ngOnInit() {

  }

  save() {
    this.navCtrl.push(HomeMenuPage);
  }

}
