import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { TruckProvider } from '../../providers/truck/truck';
import { File } from '@ionic-native/file';
import { EmailProvider } from '../../providers/email/email';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(
    public emailProvider: EmailProvider,
    public file: File,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public truckProvider: TruckProvider,
    public navParams: NavParams) {
    this.menuCtrl.enable(false, 'menu-material');
  }

  ngOnInit() {

  }

  Home() {
    this.navCtrl.push(HomePage);
  }

  OpenFacebook() {
    window.open('https://apps.facebook.com/', '_system', 'location=yes');
  };
}
