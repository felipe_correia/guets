import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { ProfileInfoPage } from '../profile-info/profile-info';
import { MyReviewsPage } from '../my-reviews/my-reviews';
import { NotificationTradePage } from '../notification-trade/notification-trade';
import { WishlistPage } from '../wishlist/wishlist';
import { MarketingPage } from '../marketing/marketing';
import { FeedPage } from '../feed/feed';

/**
 * Generated class for the confirm-exclusivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-confirm-exclusive',
  templateUrl: 'confirm-exclusive.html',
})
export class ConfirmExclusivePage {
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
  }

  feed() {
    this.navCtrl.push(FeedPage);
  }

}
