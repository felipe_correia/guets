import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { TopGuetsPage } from '../top-guets/top-guets';

/**
 * Generated class for the my-guetsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-guets',
  templateUrl: 'my-guets.html',
})
export class MyGuetsPage {
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
  }

  topGuets() {
    this.navCtrl.push(TopGuetsPage);
  }
}
