import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { ProfileInfoPage } from '../profile-info/profile-info';
import { MyReviewsPage } from '../my-reviews/my-reviews';
import { NotificationTradePage } from '../notification-trade/notification-trade';
import { WishlistPage } from '../wishlist/wishlist';
import { MarketingPage } from '../marketing/marketing';

/**
 * Generated class for the tipsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tips',
  templateUrl: 'tips.html',
})
export class TipsPage {
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
  }

  ProfileInfo() {
    this.navCtrl.push(ProfileInfoPage);
  }
  review() {
    this.navCtrl.push(MyReviewsPage);
  }
  wishlist() {
    this.navCtrl.push(WishlistPage);
  }
  notification() {
    this.navCtrl.push(NotificationTradePage);
  }
  marketing() {
    this.navCtrl.push(MarketingPage);
  }

}
