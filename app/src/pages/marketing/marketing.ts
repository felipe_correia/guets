import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { TipsPage } from '../tips/tips';
import { PostPage } from '../post/post';
import { PromotionPage } from '../post-promotion/post-promotion';
import { PostLotteryPage } from '../post-lottery/post-lottery';
import { PostExclusivePage } from '../post-exclusive/post-exclusive';
import { PostGiftPage } from '../post-gift/post-gift';

/**
 * Generated class for the HomeMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-marketing',
  templateUrl: 'marketing.html',
})
export class MarketingPage {


  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
  }


  tips() {
    this.navCtrl.push(TipsPage);
  }

  post() {
    this.navCtrl.push(PostPage);
  }

  promotion() {
    this.navCtrl.push(PromotionPage);
  }

  lottery() {
    this.navCtrl.push(PostLotteryPage);
  }

  exclusive() {
    this.navCtrl.push(PostExclusivePage);
  }

  gift() {
    this.navCtrl.push(PostGiftPage);
  }
}
