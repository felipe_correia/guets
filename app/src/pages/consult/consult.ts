import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Spinner } from 'ionic-angular';
import { TruckerModel } from '../../Models/trucker';
import { searchTruckerModel } from '../../Models/search-trucker';
import { AdminProvider } from '../../providers/admin/admin';

/**
 * Generated class for the ConsultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-consult',
  templateUrl: 'consult.html',
})
export class ConsultPage {
  // groceries: any;
  // trucker1: TruckerModel = new TruckerModel();
  // trucker2: TruckerModel = new TruckerModel();
  // trucker3: TruckerModel = new TruckerModel();
  searchTrucker: searchTruckerModel = new searchTruckerModel();
  initialDateNull: boolean;
  initialDateValidation: boolean = true;
  finalDateValidation: boolean = true;
  dateValidation: boolean = true;
  buttonValid: boolean = true;
  objectData: any;
  jsonConverter: any;
  page: number;
  initialDate: Date;
  finalDate: Date;
  initialDateCalc: number;
  finalDateCalc: number;
  result: any;
  totalTruckers: number;
  eventsOnce: any[] = [];
  loading: any = undefined;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public adminProvider: AdminProvider,
    public loadingCtrl: LoadingController
  ) {
  }

  ngOnInit() {
    this.searchRegistration();
  }

  validDate() {
    let today = new Date();
    let initialDate = new Date(this.searchTrucker.initialDate)
    if (initialDate > today) {
      this.initialDateValidation = false;
      this.dateValidation = true;
      this.finalDateValidation = true;
      return
    }

    let finalDate = new Date(this.searchTrucker.finalDate)
    if (finalDate > today) {
      this.finalDateValidation = false;
      this.dateValidation = true;
      this.initialDateValidation = true;
      return
    }

    if (this.searchTrucker.initialDate > this.searchTrucker.finalDate) {
      this.dateValidation = false;
      this.finalDateValidation = true;
      this.initialDateValidation = true;
      return
    }

    this.dateValidation = true;
    this.finalDateValidation = true;
    this.initialDateValidation = true;
    return
  }

  validButton() {
    if (!this.dateValidation) {
      this.buttonValid = false;
      return
    }

    if (!this.finalDateValidation) {
      this.buttonValid = false;
      return
    }

    if (!this.initialDateValidation) {
      this.buttonValid = false;
      return

    } else {
      this.buttonValid = true;
    }
  }



  async searchRegistration() {

    let waiting = this.loadingCtrl.create({
      spinner: 'circles',
      content: `
        <div>
        Aguarde um momento...
        </div>`,
      duration:  3000
    });
    waiting.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    waiting.present();

    this.loading = this.adminProvider.postTruckerStorage(this.searchTrucker).subscribe(
      data => {

        this.objectData = Object(data);
        this.jsonConverter = JSON.parse(this.objectData._body);

        for (let i = 0; i < this.jsonConverter.length; i++) {
          // console.log(this.jsonConverter[i].eventName);
          let n = this.eventsOnce.indexOf(this.jsonConverter[i].eventName)
          if (n == -1) {
            this.eventsOnce.push(this.jsonConverter[i].eventName);
          }
        }


      }, error => {
        console.log(error)
      }
    )

    // if (this.loading != undefined){
    //   this.searchRegistration();
    // }

  }


  searchByDate(page) {
    this.page = page;

    var query = undefined;
    query = { "page": page }

    if (this.initialDate && this.finalDate) {
      this.initialDateCalc = Date.parse(this.initialDate.toString());
      this.finalDateCalc = Date.parse(this.finalDate.toString())
      var initialDate = this.initialDateCalc - 10800000;
      var finalDate = this.finalDateCalc + 75599999;
      query = { "initialDate": initialDate, "finalDate": finalDate }

    } else if (this.initialDate && this.finalDate) {

      this.initialDateCalc = Date.parse(this.initialDate.toString());
      this.finalDateCalc = Date.parse(this.finalDate.toString())
      var initialDate = this.initialDateCalc - 10800000;
      var finalDate = this.finalDateCalc + 75599999;
      query = { "initialDate": initialDate, "finalDate": finalDate, "page": page }

    } else if (this.initialDate) {
      this.initialDateCalc = Date.parse(this.initialDate.toString());
      var initialDate = this.initialDateCalc - 10800000;
      query = { "initialDate": initialDate, "page": page }

    } else if (this.finalDate) {
      this.finalDateCalc = Date.parse(this.finalDate.toString())
      var finalDate = this.finalDateCalc + 75599999;
      query = { "finalDate": finalDate, "page": page }

    } else if (this.initialDate) {
      this.initialDateCalc = Date.parse(this.initialDate.toString());
      var initialDate = this.initialDateCalc - 10800000;
      query = { "initialDate": initialDate, "page": page }

    } else if (this.finalDate) {

      this.finalDateCalc = Date.parse(this.finalDate.toString());
      var finalDate = this.finalDateCalc + 75599999;
      query = { "finalDate": finalDate, "page": page }

    }

    this.adminProvider.searchTruckerStorageByDate(query).subscribe(r => {
      this.result = [];
      for (let cr of r) {
        let c = cr;
        this.result.push(c);
        // console.log(c);
      }
      this.totalTruckers = r.count;
    });
  }
}
