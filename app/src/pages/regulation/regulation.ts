import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TruckProvider } from '../../providers/truck/truck';

@Component({
    selector: 'page-regulation',
    templateUrl: 'regulation.html'
})
export class RegulationPage {
    status: boolean;

    constructor(public navCtrl: NavController,
        private truckprovider: TruckProvider) {

    }

    ngOnInit() {

    }


    closeRegulationTerms() {
        this.navCtrl.pop();
    }

}
