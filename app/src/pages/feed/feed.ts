import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';
import { ProfileDetailsPage } from '../profile-details/profile-details';
import { TruckProvider } from '../../providers/truck/truck';
import { File } from '@ionic-native/file';
import { EmailProvider } from '../../providers/email/email';
import { PostWishPage } from '../post-wish/post-wish';
import { AcceptExclusivePage } from '../accept-exclusive/accept-exclusive';

@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html'
})
export class FeedPage {

  constructor(
    public emailProvider: EmailProvider,
    public file: File,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public truckProvider: TruckProvider,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {

  }
  Notification() {
    this.navCtrl.push(NotificationPage);
  }

  ProfileDetails() {
    this.navCtrl.push(ProfileDetailsPage);
  }
  postWish() {
    this.navCtrl.push(PostWishPage);
  }
  acceptExclusive() {
    this.navCtrl.push(AcceptExclusivePage);
  }
}
