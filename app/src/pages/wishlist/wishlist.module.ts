import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular/umd';
import { WishlistPage } from './wishlist';
import { RegisterPage } from '../register/register';

@NgModule({
  declarations: [
    WishlistPage,
  ],
  imports: [
    IonicPageModule.forChild(WishlistPage),
    RegisterPage
  ],
})
export class WishlistPageModule { }
