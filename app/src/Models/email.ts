export class EmailModel {
    to: string;
    subject: string;
    text: string;
    file: any;

    constructor() {
        this.to = "";
        this.subject = "";
        this.text = "";
    }

}