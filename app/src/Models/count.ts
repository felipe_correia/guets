export class CountModel {
    _id: string;
    date: Date;
    count: number;
    id_device: string;



    constructor() {
        this.count = 0;
        this.id_device = "";
     
    }

    public loadTrucker(response: any) {
        this._id = response._id;
        this.date = response.date;
        this.count = response.count;
        this.id_device = response.id_device;
   
    }
}